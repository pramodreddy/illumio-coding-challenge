# Illumio-Coding Challenge

Illumio Coding Assignment 2018, PCE teams

It took me about 90 mins to come up with this implementation. It was indeed a challenging but interesting problem. I have addressed the following input rules:

direction – either “inbound” or “outbound”
protocol – either “tcp” or “udp”
port – a) an integer b) ambiguity related to the port range which is addressed below
IP address – a) an ip address b) range of two ip addresses.
I have considered the following input through a CSV file: (input.csv)
inbound,tcp,80,192.168.1.2 
outbound,tcp,10000-20000,192.168.10.11 
inbound,udp,53,192.168.1.1-192.168.2.5 
outbound,udp,1000-2000,52.12.48.92

Due to time limitation, I could not cover every implementation mentioned in the problem. The problem expects me to match all packets within the range of a port or an ip address when a range for the same is passed. I completed port range validations but unfortunately I fell short of time and was only able to validate the input for the passed starting and ending value and not for the values that lie within them. With more time at hand, I would have stored the values by setting a counter from the starting value to the ending value inclusive of both.

Design Decisions:

As there are 4 parameters for each rule to pass. Keeping performance in mind the started with direction and protocol matching which will elminate all other rules which are not matching. Out the remaining rules then I started checking port and I checked ranged port if port is within the range I verified ip address Similarly I followed for the single port.

Testing:
I have used Junit4 for the testcase and wrote 4 sample test cases.

I would love to be a part of the Platform Team.

