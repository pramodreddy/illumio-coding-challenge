package com.firewall.filter;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class FirewallTest {
	
	 static Firewall f = null;
	 
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		f = new Firewall("/C:/Logtesting/input.csv");
	}
	
	@Test
	public void testAcceptPacket() {
		boolean acceptPacket = f.accept_packet("inbound", "tcp", 80, "192.168.1.2");
		assertEquals("This has to be true", true, acceptPacket);
	}
	
	@Test
	public void testAcceptPacket1() {
		boolean acceptPacket = f.accept_packet("outbound", "tcp", 10100, "192.168.10.11");
		assertEquals("This has to be true", true, acceptPacket);
	}
	
	@Test
	public void testAcceptPacket2() {
		boolean acceptPacket = f.accept_packet("inbound", "udp", 53, "192.168.1.10");
		assertEquals("This has to be true", true, acceptPacket);
	}
	
	@Test
	public void testAcceptPacket3() {
		boolean acceptPacket = f.accept_packet("outbound", "udp", 1010, "52.12.48.92");
		assertEquals("This has to be true", true, acceptPacket);
	}
	
}
